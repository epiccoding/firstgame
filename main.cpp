#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "variables.h"


using namespace std;

int main(int argc, char *argv[]) {
    SDL_Window *win = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Texture *bitmapTex = NULL;
    SDL_Surface *bitmapSurface = NULL;
    int posX = 100, posY = 100, width = 1080, height = 720;

    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);


    win = SDL_CreateWindow("100% Totally accurate representation of the final product", posX, posY, width, height, SDL_WINDOW_RESIZABLE);

    renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

    bitmapSurface = SDL_LoadBMP("img/hello.bmp");
    bitmapTex = SDL_CreateTextureFromSurface(renderer, bitmapSurface);
    SDL_FreeSurface(bitmapSurface);
    SDL_Texture* texture = NULL;
    SDL_Surface * surface = NULL;
    surface = SDL_LoadBMP("img/Sprites/Bobette/bobette.bmp");
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_Surface* ws = SDL_LoadBMP("player.bmp");
    SDL_Texture* wt = SDL_CreateTextureFromSurface(renderer, ws);
    SDL_Rect textureRect = { 0,0,200,200 };
    while (running) {
        SDL_WaitEvent(&event);
        switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_LEFT: movementkeys[0] = true; break;
                    case SDLK_RIGHT: movementkeys[1] = true; break;
                    case SDLK_UP: movementkeys[2] = true; break;
                    case SDLK_DOWN: movementkeys[3] = true; break;
                }
                break;
            case SDL_KEYUP:
                switch (event.key.keysym.sym) {
                    case SDLK_LEFT: movementkeys[0] = false; break;
                    case SDLK_RIGHT: movementkeys[1] = false; break;
                    case SDLK_UP: movementkeys[2] = false; break;
                    case SDLK_DOWN: movementkeys[3] = false; break;
                }
                break;

        }
        if (movementkeys[0] == true) {
            if (xspeed > -5) {
            xspeed = xspeed - 1;
        }
        }
        else if (movementkeys[1] == true) {
            if(xspeed < 5) {
            xspeed = xspeed + 1;
        }
        }
        if (movementkeys[2] == true) {
            if (yspeed > -5) {
            yspeed = yspeed - 1;
        }
        }
        else if (movementkeys[3] == true) {
            if (yspeed < 5) {
            yspeed = yspeed + 1;
        }
        }


        if (movementkeys[0] == false && movementkeys[1] == false) {
            xspeed = 0;
        }
        if (movementkeys[2] == false && movementkeys[3] == false) {
            yspeed = 0;
        }



        x = x + xspeed;
        y = y + yspeed;

        SDL_Rect playerRect = { x,y,50,50 };
        SDL_Rect wallRect = { 200,200,10,100 };


        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, texture, &textureRect, &playerRect);
        SDL_RenderCopy(renderer, wt, &textureRect, &wallRect);
        SDL_RenderPresent(renderer);
}
    SDL_DestroyTexture(bitmapTex);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);

    SDL_Quit();

    return 0;



}